ARG IMAGE_TAG=latest
FROM registry.gitlab.com/epicsoft-networks/ansible:$IMAGE_TAG

#### https://galaxy.ansible.com/ui/repo/published/ansible/netcommon/
RUN ansible-galaxy collection install ansible.netcommon

#### https://galaxy.ansible.com/community/hashi_vault
#### https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/hashi_vault_lookup.html
RUN ansible-galaxy collection install community.hashi_vault

#### https://github.com/TerryHowe/ansible-modules-hashivault
#### https://terryhowe.github.io/ansible-modules-hashivault/modules/list_of_hashivault_modules.html
RUN ansible-galaxy install 'git+https://github.com/TerryHowe/ansible-modules-hashivault.git' \
 && python3 -m pip install ansible-modules-hashivault

#### https://docs.ansible.com/ansible/latest/collections/ansible/posix/authorized_key_module.html
#### https://galaxy.ansible.com/ui/repo/published/ansible/posix/
RUN ansible-galaxy collection install ansible.posix

#### https://galaxy.ansible.com/ui/repo/published/community/general/
RUN ansible-galaxy collection install community.general
