#### https://hub.docker.com/_/python
FROM python:3.12.2-alpine3.18

ARG BUILD_DATE
LABEL org.label-schema.name="Ansible" \
      org.label-schema.description="Ansible based on Alpine Linux" \
      org.label-schema.vendor="epicsoft LLC / Alexander Schwarz <as@epicsoft.one>" \
      org.label-schema.version="3.18" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=${BUILD_DATE}

LABEL image.name="epicsoft_ansible" \
      image.description="Ansible based on Alpine Linux" \
      maintainer="epicsoft LLC" \
      maintainer.name="Alexander Schwarz <as@epicsoft.one>" \
      maintainer.copyright="Copyright 2020-2024 epicsoft LLC / Alexander Schwarz" \
      license="MIT"

ENV EDITOR=nano

RUN apk --no-cache add sshpass \
                       openssh-client \
                       openssl \
                       rsync \
                       nano \
                       py-pip \
                       py3-setuptools \
                       git \
                       jq \
                       curl \
                       yq \
                       ca-certificates \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*

RUN python3 -m pip install --upgrade pip \
 && python3 -m pip install ansible==9.2.0 \
                           ansible-lint==24.2.0

RUN python3 -m pip install hvac==2.1.0 \
                           botocore==1.34.44 \
                           boto3==1.34.44 \
                           resolvelib==0.8.1

ENTRYPOINT []

CMD [ "ansible", "--help" ]
